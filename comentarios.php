<!DOCTYPE html>
<html>
  <title>Comentarios</title>
  
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allerta+Stencil">
  <style>
    .w3-allerta {
      font-family: "Allerta Stencil", sans-serif;
    }
  </style>

  <body class="w3-green">
    <div class='w3-display-topleft w3-margin'>
      <form method='post' action='./index.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='inicio' class='w3-button w3-black w3-padding'>Inicio</button>
      </form>
    </div>

    <div class='w3-display-topright w3-margin'>
      <form method='post' action='./logout.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='logout' class='w3-button w3-black w3-padding'>Sair</button>
      </form>
    </div>

    <div class="w3-container w3-card-4 w3-light-grey w3-allerta w3-margin w3-display-topmiddle w3-padding-16" style="width: 40%;">
    
    <?php
        $email_cookie = $_COOKIE['email'];

        if ($email_cookie) {
            echo "<form method='get' action='#' class='w3-allerta w3-padding-16'>";
            //echo "<label>Digite seu nome:</label>";
            //echo "<input class='w3-input w3-border w3-margin-bottom' name='nome' type='text' placeholder='Nome' required>";
            echo "<label>Digite seu comentário:</label>";
            echo "<input class='w3-input w3-border w3-margin-bottom' name='comentario' type='text' placeholder='Comentário' required>";
            echo "<button type='submit' name='comentar' class='w3-button w3-black w3-padding w3-right'>Comentar</button>";
            echo "</form>";

            echo "<div class='w3-container w3-allerta w3-padding-8'>";

            include './dataBaseMySql.php';
            
            // Nome do banco de dadosS
            $dbname = "app_php_seg";
            $table = "comments";

            // Conexão com o banco 'app_php_seg'
            $conn = connection();
            
            // seleciona o banco 'app_php_seg'
            mysqli_select_db($conn, $dbname);
            
            $query = "SELECT * FROM $table";
            $result = mysqli_query($conn, $query) or die ("Erro na consulta !");
            
            if(mysqli_num_rows($result) > 0) {

                while($row = mysqli_fetch_array($result)){
                    echo "Nome: " . $row["name"] . "<br>";
                    echo "Comentario: " . $row["comment"] . "<br>" . "<br>";
                }
            }
            
            // echo "</div>";

            if (isset($_GET['comentar'])) {
		
		$query_nome = "SELECT firstname FROM users WHERE email='$email_cookie'";
		$result = mysqli_query($conn, $query_nome) or die ("Erro na consulta !");
		
		$nome;
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_array($result)){
				global $nome;
                        	$nome  = $row["firstname"];
	            	}
		}
		
                //$comentario = htmlspecialchars($_GET['comentario']);
		$comentario = $_GET['comentario'];
                
                $query = "INSERT INTO comments (name, comment) VALUES ('$nome', '$comentario')";
                $result = mysqli_query($conn, $query) or die ("Erro no cadastro do comentario !");

                header("Location:./comentarios.php");
            }

            mysqli_close($conn);

            echo "</div>";
        }
    ?>
    </div>
  </body>
</html>
