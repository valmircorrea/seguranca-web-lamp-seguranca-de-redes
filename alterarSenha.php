<!DOCTYPE html>
<html>
  <title>Alteracao de Senha</title>
  
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allerta+Stencil">
  <style>
    .w3-allerta {
      font-family: "Allerta Stencil", sans-serif;
    }
  </style>

  <body class="w3-green">
    <div class='w3-display-topleft w3-margin'>
      <form method='post' action='./index.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='inicio' class='w3-button w3-black w3-padding'>Inicio</button>
      </form>
    </div>

    <div class='w3-display-topright w3-margin'>
      <form method='post' action='./logout.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='logout' class='w3-button w3-black w3-padding'>Sair</button>
      </form>
    </div>

    <div class="w3-container w3-card-4 w3-light-grey w3-allerta w3-margin w3-display-topmiddle w3-padding-16" style="width: 40%;">
    
    <?php
        $email_cookie = $_COOKIE['email'];

        if ($email_cookie) {
            echo "<form method='get' action='#' class='w3-allerta w3-padding-16'>";
            echo "<label>Nova senha:</label>";
            echo "<input class='w3-input w3-border w3-margin-bottom' name='nova_senha' type='password' required>";
            echo "<label>Confirme a nova senha:</label>";
            echo "<input class='w3-input w3-border w3-margin-bottom' name='nova_senha_confirm' type='password' required>";
            echo "<button type='submit' name='alterar' class='w3-button w3-black w3-padding w3-right'>Alterar</button>";
            echo "</form>";
            

            if (isset($_GET['alterar'])) {

		include './dataBaseMySql.php';

            	$nova_senha = MD5($_GET['nova_senha']);
		$nova_senha_confirm = MD5($_GET['nova_senha_confirm']);

		if ($nova_senha !== $nova_senha_confirm) {
		    echo "<script language='javascript' type='text/javascript'>alert('Confirmação de senha incorreto !');window.location.href='./alterarSenha.php'</script>";
		}
            
            	$dbname = "app_php_seg";
            	$table = "users";
		
		$conn = connection();

		mysqli_select_db($conn, $dbname);
		
		$query = "UPDATE $table SET pswd='$nova_senha' WHERE email='$email_cookie'";
		$result = mysqli_query($conn, $query) or die ("Erro na alteração da senha !");
		
		mysqli_close($conn);

		echo "<script language='javascript' type='text/javascript'>alert('Senha alterada com sucesso !');window.location.href='./index.php'</script>";
            }
        }
    ?>
    </div>
  </body>
</html>
