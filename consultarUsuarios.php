<!DOCTYPE html>
<html>
  <title>Consulta Usuarios</title>
  
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allerta+Stencil">
  <style>
    .w3-allerta {
      font-family: "Allerta Stencil", sans-serif;
    }
  </style>

  <body class="w3-green">
    <div class='w3-display-topleft w3-margin'>
      <form method='post' action='./index.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='inicio' class='w3-button w3-black w3-padding'>Inicio</button>
      </form>
    </div>

    <div class='w3-display-topright w3-margin'>
      <form method='post' action='./logout.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='logout' class='w3-button w3-black w3-padding'>Sair</button>
      </form>
    </div>

    <div class="w3-container w3-card-4 w3-light-grey w3-allerta w3-margin w3-display-topmiddle w3-padding-16" style="width: 35%;">
    
    <?php
        $email_cookie = $_COOKIE['email'];

        if ($email_cookie) {
            echo "<form method='get' action='#' class='w3-allerta w3-padding-16'>";
            echo "<label>Digite o ID do usuário</label>";
            //echo "<input class='w3-input w3-border w3-margin-bottom' name='userId' type='number' placeholder='ID'>";
            echo "<input class='w3-input w3-border w3-margin-bottom' name='userId' type='text' placeholder='ID'>";
	    echo "<button type='submit' name='consulta' class='w3-button w3-black w3-padding w3-right'>Consultar</button>";
            echo "</form>";

            echo "<div class='w3-container w3-allerta w3-padding-8'>";
        }

        if (isset($_GET['consulta'])) {
            include './dataBaseMySql.php';
 
 	    $id = $_GET['userId'];
           
            echo "<label><i>Você buscou por: " .$id ."</i></label><br><br>";   
            // Nome do banco de dadosS
            $dbname = "app_php_seg";
            $table = "users";
    
            // Conexão com o banco 'app_php_seg'
            $conn = connection();
            
            // seleciona o banco 'app_php_seg'
            mysqli_select_db($conn, $dbname);
            
            $query = "SELECT id, firstname, email FROM $table WHERE id='$id'";
            $result = mysqli_query($conn, $query) or die ("Erro na consulta !");
            
            if(mysqli_num_rows($result) > 0) {
    
                while($row = mysqli_fetch_array($result)){
                    echo "Id: " . $row["id"] . "<br>";
                    echo "Nome: " . $row["firstname"] . "<br>";
                    echo "Email: " . $row["email"] . "<br>" . "<br>";
                }
            }
            
            echo "</div>";

            mysqli_close($conn);
        }
    ?>
    </div>
  </body>
</html>
