<!DOCTYPE html>
<html>
  <title>Inicio</title>
  
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allerta+Stencil">
  <style>
    .w3-allerta {
      font-family: "Allerta Stencil", sans-serif;
    }
  </style>

  <body class="w3-green">
    <div class='w3-display-topright w3-margin'>
      <form method='get' action='./logout.php' class='w3-allerta w3-padding-16'>
        <button type='submit' name='logout' class='w3-button w3-black w3-padding'>Sair</button>
      </form>
    </div>

    <div class="w3-container w3-card-4 w3-light-grey w3-allerta w3-display-middle w3-padding-16" style="width: 35%;">
    
    <?php
      $email_cookie = $_COOKIE['email'];

      if ($email_cookie) {
        echo "<h1 class=' w3-center w3-allerta'>Bem vindo !</h1>";
        echo "<a href='./consultarUsuarios.php'> <input type='button' value='Consultar Usuários' class='w3-button w3-black w3-left'/> </a>";
        echo "<a href='./comentarios.php'> <input type='button' value='Comentários' class='w3-button w3-black w3-margin-left w3-margin-right w3-margin-bottom w3-display-bottommiddle'/> </a>";
        echo "<a href='./alterarSenha.php'> <input type='button' value='Alterar Senha' class='w3-button w3-black w3-right'/> </a>";
      }
    ?>
    </div>
  </body>
</html>
